# [clipperz-docker](https://gitlab.com/misuzu/clipperz-docker)
Docker image for painlessly deploying [Clipperz](https://clipperz.is) service.
Image includes [delta](https://github.com/clipperz/password-manager/tree/master/frontend/delta) frontend and [Flask](https://github.com/clipperz/password-manager/tree/master/backend/flask) backend.
Based on [jokajak](https://github.com/jokajak/password-manager) changes.

To test it out run this and go to http://localhost:8000. **All data will be lost on container exit!**
```bash
docker run --rm -it \
    --name clipperz \
    -p 8000:8000 \
    -e GUNICORN_BIND="0.0.0.0:8000" \
    -e SQLALCHEMY_DATABASE_URI="sqlite:////tmp/database.sqlite3" \
    misuzu/clipperz
```

For persistent sqlite3 database try something like this:
```bash
docker run -d \
    --name clipperz \
    --restart=unless-stopped \
    -v /var/lib/clipperz:/var/lib/clipperz \
    -p 8000:8000 \
    -e GUNICORN_BIND="0.0.0.0:8000" \
    -e SQLALCHEMY_DATABASE_URI="sqlite:////var/lib/clipperz/database.sqlite3" \
    misuzu/clipperz
```

More production-like deployment using unix-sockets with nginx and postgresql instead of sqlite3 would be something like this:
```bash
docker run -d \
    --name clipperz \
    --restart=unless-stopped \
    -v /run/postgresql:/run/postgresql \
    -v /run/clipperz:/run/clipperz \
    -e GUNICORN_BIND="unix:/run/clipperz/gunicorn" \
    -e SQLALCHEMY_DATABASE_URI="postgresql://user:pass@/database" \
    misuzu/clipperz
```

nginx:
```
server {
    listen 80;
    listen [::]:80;
    server_name clipperz.example.com;
    location / {
        proxy_pass http://unix:/run/clipperz/gunicorn;
    }
}
```

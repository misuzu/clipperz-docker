#!/usr/bin/env python
from __future__ import print_function
import setuptools
import io
import os
import sys

here = os.path.abspath(os.path.dirname(__file__))


def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    try:
        for filename in filenames:
            with io.open(filename, encoding=encoding) as f:
                buf.append(f.read())
    except IOError:
        pass
    return sep.join(buf)

long_description = read('README.txt', 'CHANGES.txt')


setuptools.setup(
    name='clipperz',
    version='0.1.0',
    url='http://github.com/clipperz/password-manager/',
    license='Apache Software License',
    author='Jokajak',
    tests_require=['pytest'],
    install_requires=['Flask>=0.10.1,<0.12',
                      'Werkzeug>=0.16.1,<0.17',
                      'Flask-SQLAlchemy>=1.0,<2.2',
                      'SQLAlchemy>=0.8.2,<1.2',
                      'Flask-Login>=0.4,<0.5',
                      'Flask-KVSession>=0.6,<0.7',
                      'Flask-Migrate<=2.5.2,<2.6',
                      'Flask-Script<=2.0.6,<2.1',
                      'psycopg2-binary>=2.8,<2.9',
                      ],
    author_email='jokajak@gmail.com',
    description='Clipperz password manager server',
    long_description=long_description,
    packages=setuptools.find_packages(
        exclude=['*.tests', '*.tests.*', 'tests.*', 'tests']),
    include_package_data=True,
    platforms='any',
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)

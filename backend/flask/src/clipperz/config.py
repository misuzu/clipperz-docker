import os
import datetime


class Production(object):
    def __init__(self):
        self.DEBUG = False
        self.TESTING = False
        self.CSRF_ENABLED = True
        self.WTF_CSRF_ENABLED = True
        self.SECRET_KEY = os.urandom(32)
        self.sessionTimeout = datetime.timedelta(minutes=-2)

        self.SQLALCHEMY_ECHO = False
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False
        self.SQLALCHEMY_DATABASE_URI = os.environ['SQLALCHEMY_DATABASE_URI']


class Development(object):
    def __init__(self):
        self.DEBUG = True
        self.TESTING = False
        self.CSRF_ENABLED = True
        self.WTF_CSRF_ENABLED = True
        self.SECRET_KEY = os.urandom(32)

        self.SQLALCHEMY_ECHO = False
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False
        self.SQLALCHEMY_RECORD_QUERIES = True
        self.SQLALCHEMY_DATABASE_URI = (
            'sqlite:///' + os.path.join(
              os.path.dirname(os.path.abspath(__file__)), 'app.db') +
            '?check_same_thread=False')

class Testing(Production):
    def __init__(self):
        super(Testing, self).__init__()
        self.TESTING = True


def get_config():
    name = os.environ.get('FLASK_ENV', 'development')
    return {
        'production': Production,
        'development': Development,
        'testing': Testing,
    }[name]()

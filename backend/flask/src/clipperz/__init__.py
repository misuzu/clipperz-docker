import os

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_kvsession import KVSessionExtension

from simplekv.db.sql import SQLAlchemyStore

from config import get_config

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__, static_url_path='')

lm = LoginManager()
lm.init_app(app)

app.config.from_object(get_config())

db = SQLAlchemy(app)

store = SQLAlchemyStore(db.engine, db.metadata, 'sessions')
kvsession = KVSessionExtension(store, app)

from clipperz import views, models, api

FROM debian:buster-slim as builder

RUN apt-get update

RUN apt-get install -y ca-certificates python python-setuptools
RUN apt-get install -y build-essential python-dev virtualenv
RUN apt-get install -y git

COPY . /app
WORKDIR /app

RUN virtualenv -p python2 /venv && . /venv/bin/activate && \
    pip install "GitPython>=2.1,<2.2" && \
    bash /app/scripts/build install --backends flask --frontends beta delta && \
    mv /app/target/flask/delta /app/target/flask/clipperz/static/ && \
    pip install /app/target/flask "gunicorn[gevent]>=19.9,<19.10"


FROM debian:buster-slim as final

RUN apt-get update

RUN apt-get install -y ca-certificates python python-setuptools

RUN apt-get clean

COPY --from=builder /venv /venv

ENV FLASK_ENV="production"

ENV GUNICORN_BIND="0.0.0.0:8000"
ENV GUNICORN_OPTIONS=""

CMD ["/bin/bash", "-c", ". /venv/bin/activate && python -m clipperz.manage db upgrade && exec gunicorn clipperz:app -k gevent -b ${GUNICORN_BIND} ${GUNICORN_OPTIONS}"]
